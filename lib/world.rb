require_relative 'cell'
# World in which cells live
class World
  attr_accessor :cells, :dead_cells, :width, :height, :life_factor

  def initialize(width = 20, height = 20, life_factor = 0.2)
    @cells = {}
    @dead_cells = {}
    @life_factor = life_factor
    @width = width
    @height = height
  end

  def tick!
    all_cells.each_value do |cell|
      cell.die! if cell.nb_neighbours < 2 || cell.nb_neighbours > 3
      cell.live! if cell.nb_neighbours == 3 && cell.dead?
    end
  end

  def populate
    (0...width).each do |x|
      (0...height).each do |y|
        if rand < life_factor
          @cells["#{x}, #{y}"] = Cell.new(self, x, y)
        else
          @dead_cells["#{x}, #{y}"] = Cell.new(self, x, y)
        end
      end
    end
  end

  def all_cells
    @cells.merge(@dead_cells)
  end

  def draw
    # cheat
    system('clear')

    # draw the board
    (0...width).each do |x|
      (0...height).each do |y|
        print @cells["#{x}, #{y}"] ? '*' : ' '
      end
      puts
    end
  end
end
