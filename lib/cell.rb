# the living organism
class Cell
  attr_accessor :world, :x, :y

  def initialize(world, x = 0, y = 0)
    @x = x
    @y = y
    @world = world
  end

  def spawn_at(x, y)
    cell = Cell.new(world, x, y)
    world.cells[cell.to_s] = cell
  end

  def nb_neighbours
    neighbours.count
  end

  def neighbours
    neighbours = []
    world.cells.each_value do |cell|
      neighbours << cell if within_touch(cell) && cell != self
    end
    neighbours
  end

  def die!
    world.cells.delete(to_s)
    world.dead_cells[to_s] = self
  end

  def live!
    world.dead_cells.delete(to_s)
    world.cells[to_s] = self
  end

  def dead?
    !alive?
  end

  def alive?
    world.cells[to_s] == self
  end

  def to_s
    "#{x}, #{y}"
  end

  private

  def within_touch(cell)
    (x - cell.x).abs <= 1 && (y - cell.y).abs <= 1
  end
end
